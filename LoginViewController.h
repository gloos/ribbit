//
//  LoginViewController.h
//  Ribbit
//
//  Created by Gary Luce on 21/11/2013.
//  Copyright (c) 2013 Gary Luce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController 
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
- (IBAction)login:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end
