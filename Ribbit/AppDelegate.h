//
//  AppDelegate.h
//  Ribbit
//
//  Created by Gary Luce on 17/11/2013.
//  Copyright (c) 2013 Gary Luce. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
