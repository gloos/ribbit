//
//  FriendsViewController.h
//  Ribbit
//
//  Created by Gary Luce on 01/12/2013.
//  Copyright (c) 2013 Gary Luce. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface FriendsViewController : UITableViewController

@property (strong, nonatomic) PFRelation *friendsRelation;
@property (strong, nonatomic) NSArray *friends;

@end
